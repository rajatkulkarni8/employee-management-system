from employee_data import Employee
from company import Company

def print_menu():
    print("\nEmployee Management System")
    print("1. Add Department")
    print("2. Remove Department")
    print("3. Display All Departments")
    print("4. Add Employee")
    print("5. Remove Employee")
    print("6. Display Employee Details")
    print("7. Exit")

def main():
    company = Company()

    while True:
        print_menu()
        choice = input("Enter your choice: ")

        if choice == '1':
            dept_name = input("Enter department name: ")
            company.add_department(dept_name)

        elif choice == '2':
            dept_name = input("Enter department name to remove: ")
            company.remove_department(dept_name)

        elif choice == '3':
            company.display_departments()

        elif choice == '4':
            dept_name = input("Enter department name: ")
            department = company.get_department(dept_name)
            if department:
                emp_name = input("Enter employee name: ")
                emp_id = input("Enter employee ID: ")
                emp_title = input("Enter employee title: ")
                employee = Employee(emp_name, emp_id, emp_title, dept_name)
                department.add_employee(employee)
            else:
                print(f"Department '{dept_name}' does not exist.")

        elif choice == '5':
            dept_name = input("Enter department name: ")
            department = company.get_department(dept_name)
            if department:
                emp_id = input("Enter employee ID to remove: ")
                department.remove_employee(emp_id)
            else:
                print(f"Department '{dept_name}' does not exist.")

        elif choice == '6':
            emp_id = input("Enter employee ID to display details: ")
            company.display_employee(emp_id)

        elif choice == '7':
            print("Exiting Employee Management System.")
            break

        else:
            print("Invalid choice. Please try again.")

if __name__ == "__main__":
    main()
