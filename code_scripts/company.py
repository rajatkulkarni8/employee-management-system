from department import Department

class Company:
    def __init__(self):
        self.departments = {}

    def add_department(self, name):
        if name not in self.departments:
            self.departments[name] = Department(name)
            print(f"Department '{name}' added.")
        else:
            print(f"Department '{name}' already exists.")

    def remove_department(self, name):
        if name in self.departments:
            del self.departments[name]
            print(f"Department '{name}' removed.")
        else:
            print(f"Department '{name}' does not exist.")

    def display_departments(self):
        if not self.departments:
            print("No departments available.")
        else:
            for dept in self.departments.values():
                print(f"Department: {dept.name}")
                dept.list_employees()

    def get_department(self, name):
        return self.departments.get(name, None)

    def display_employee(self, emp_id):
        print("##",)
        for dept in self.departments.values():
            print("**",self.departments.values())
            emp = dept.get_employee(emp_id)
            if emp:
                emp.display_details()
                return
        print(f"No employee found with ID: {emp_id}")
